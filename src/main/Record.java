//Se charge de charger et sauvegarder le record actuel

package main;

import java.io.*;

public class Record {
	
	private static int record = 2500; //Record par defaut
	private static BufferedReader input;
	private static PrintWriter output;
	
	
	public static void init() {
		
		try {
			input = new BufferedReader (new FileReader ("record.txt"));
			record = Integer.parseInt (input.readLine ());
			input.close();
		} catch (NumberFormatException e) {
			e.printStackTrace();
			record = 2500; //Record par defaut
			System.out.println("Record could not be read, Number Format Exception");
		} catch (IOException e) {
			e.printStackTrace();
			record = 2500; //Record par defaut
			System.out.println("Record could not be read, IO Exception");
		}
	}
	
	public static int get() {
		return record; //Retourne le record actuel
	}
	
	public static void set(int _score) { //Sauvegarde un nouveau record (a noter que la verification devrait se faire avant)
		
		try {
			output = new PrintWriter (new FileWriter ("record.txt"));
			output.println(_score);
			output.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Record could not be saved, IO Exception");
		}
		
	}
	
	public static boolean check(int _score) { //Verifie si le record est battu
		
		if (_score > record) {
			return true;
		}
		else
			return false;
	}

}
