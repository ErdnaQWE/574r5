package main;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import main.levels.Tile;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
//import org.newdawn.slick.Sound;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.util.ResourceLoader;

public class Resources {
	
	private static Map<String, Image> images;
	private static Map<String, SpriteSheet> sprites;
	private static Map<String, TrueTypeFont> fonts;
	//private static Map<String, Sound> sounds;
	
	public Resources() {
		images = new HashMap<String, Image>();
		sprites = new HashMap<String, SpriteSheet>();
		fonts = new HashMap<String, TrueTypeFont>();
		//sounds = new HashMap<String, Sound>();
		
		try { //Chargement de ressources image en specifiant sa location dans le dossier res (ressources)
			sprites.put("excroissance", loadSprite("res/excroissance.png", 32, 32));
			sprites.put("dion_anim", loadSprite("res/logo/logo_anim.png", 81, 87)); //13 frames
			sprites.put("spaceship", loadSprite("res/spaceship_anim.png", 68, 74)); //4 frames
			sprites.put("speedster", loadSprite("res/speedster_anim.png", 46, 72)); //2 frames
			
			sprites.put("tileset", loadSprite("res/tileset.png", Tile.SMALL_SIZE, Tile.SMALL_SIZE));
			
			sprites.put("items", loadSprite("res/items.png", 32, 32));
			
			images.put("dion", loadImage("res/logo/logo.png"));
			images.put("dion2", loadImage("res/logo/logo2.png"));
			
			images.put("orb_vie", loadImage("res/gui/orb_vie.png"));
			images.put("orb_energie", loadImage("res/gui/orb_energie.png"));
			images.put("orb_vide", loadImage("res/gui/orb_vide.png"));
			images.put("orb_vide_inverse", loadImage("res/gui/orb_vide_inverse.png"));
			
			images.put("shooter", loadImage("res/shooter.png"));
			
			fonts.put("Mecha_Bold_64", loadFont("res/fonts/Mecha_Bold.ttf", 64f));
			fonts.put("Mecha_Condensed_Bold_16", loadFont("res/fonts/Mecha_Condensed_Bold.ttf", 16f));
			fonts.put("Mecha_Condensed_Bold_48", loadFont("res/fonts/Mecha_Condensed_Bold.ttf", 48f));
			fonts.put("Mecha_Condensed_32", loadFont("res/fonts/Mecha_Condensed.ttf", 32f));
			//fonts.put("Mecha_32", loadFont("res/fonts/Mecha.ttf", 32f));

			
		}
		catch (SlickException e) {
			e.printStackTrace();
		}
		
	}

	private static Image loadImage (String path) throws SlickException {
		return new Image (path, false, Image.FILTER_NEAREST); //Charge une image d'un fichier 
	}
	
	public static SpriteSheet getSprite(String getter) {
		return sprites.get(getter);
	}
	
	public static Image getSpriteImage(String getter, int x, int y) {
		return sprites.get(getter).getSubImage(x, y);
	}
	
	public static Image getImage (String getter) {
		return images.get(getter);
	}
	
	public static Image image(String getter) {
		return sprites.get(getter);
	}
	
	public static Image getSprite (String getter, int x, int y) {
		return sprites.get(getter).getSprite(x, y);
	}
	
	private SpriteSheet loadSprite (String path, int tw, int th) throws SlickException {
		return new SpriteSheet (loadImage(path), tw, th); //Charge un sprite d'un fichier image
	}
	
	public static TrueTypeFont loadFont (String path, float size) {
 		
		Font awtFont = null;
 		
		try {
			awtFont = Font.createFont(Font.TRUETYPE_FONT, ResourceLoader.getResourceAsStream(path));
		} catch (FontFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		awtFont = awtFont.deriveFont(size); // set font size
		return new TrueTypeFont(awtFont, false);
	}
	
	public static TrueTypeFont getFont (String getter) {
		return fonts.get(getter);
	}
}
