//Will contain all of the player's information
//Will contain methods to load and save to JSON files

package main;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Info {
	
	public int min_damage = 2;
	public int max_damage = 4;
	
	
	
	
	@SuppressWarnings("unchecked")
	public static void Save() {
		
		JSONObject obj = new JSONObject();
		obj.put("Name", "This is a test");
		obj.put("Amount", 100);
		
		try {
			 
			FileWriter file = new FileWriter("c:\\test.json");
			file.write(obj.toJSONString());
			file.flush();
			file.close();
	 
		} catch (IOException e) {
			e.printStackTrace();
		}
	 
		System.out.println(obj);
		
	}
	
	public static void Load() {
		
		JSONParser parser = new JSONParser();
		
		try {
			Object obj = parser.parse(new FileReader("c:\\test.json"));
			JSONObject jsonObject = (JSONObject) obj;
					
			String name = (String) jsonObject.get("Name");
			System.out.println(name);
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
			System.err.println("The file was not found!");
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} 
		
	}

}
