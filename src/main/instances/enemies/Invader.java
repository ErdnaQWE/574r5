//Bonhomme qui agit comme un gars dans Lost in Space I
//Le plan c'est que le monstre pourra travailler avec les autres de meme espece afin de faire comme une game de Lost in Space
//Je sais pas vraiment ce que je suis en train de faire :)
//Je vais probalement rendre son mouvement sacade pour rajouter au style... quoi?

package main.instances.enemies;

import main.Window;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class Invader extends Enemy {
	
	private int h_speed = (int) Math.round(width); //Vitesse horizontale par seconde
	private int time = 0; //Temps entre les updates
	private int UPDATE_DELAY = 1000; //Un update a chaque secondes
	
	public Invader(int x, int y, int width, int height) {
		super(x, y, width, height, 10, 150);
	}
	
	@Override
	public void update(int delta) {
		time += delta;
		
		while (time > UPDATE_DELAY) {
			
			time -= UPDATE_DELAY;
		
			x+=h_speed*(UPDATE_DELAY/1000.0);
			
			if (x < 12) {
				x = 24; //Replace l'objet a 0 si il depasse l'ecran
				y += height + 16; //Descend de un espace, comme un vrai space invader :)
				h_speed = -h_speed; // Inverse la direction
			}
			else if (x > Window.WIDTH - width - 12) {
				x = Window.WIDTH - width - 12;
				y += height + 16;
				h_speed = -h_speed; //Inverse la direction
			}
		}
	}
	
	@Override
	public void draw(Graphics g) {
		g.setColor(Color.white);
		g.drawRect(x, y, width, height);
		g.drawString("HP: " + health_current,(float) x,(float) y);
	}
	

}
