//Monstre qui accelere de maniere exponentielle

package main.instances.enemies;

import main.Resources;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class Speedster extends Enemy {
	
	private int time = 0; //Pour calculer le temps de vie du monstre
	private float v_speed = 100; //Vitesse verticale
	
	public Speedster(int x, int y, int width, int height) {
		super(x, y, width, height, 10, 200);
	}
	
	@Override
	public void draw (Graphics g) {
		
		if (hit == 0)
			g.drawImage(Resources.getSprite("speedster",(int) Math.floor((time%200)/100), 0), x, y);
		else {
			g.drawImage(Resources.getSprite("speedster",(int) Math.floor((time%200)/100), 0), x, y, Color.red);
			hit -= 1;
		}
	}
	
	@Override
	public void update (int delta) {
		
		time += delta;
		
		if (time >= 1500 || time < 2500)
			v_speed += 300 * (delta/1000.0);
		else
			v_speed += 1400 * (delta/1000.0);
		
		y += v_speed * (delta/1000.0);
	}
}
