//Template de base pour tout les "ennemis"
//Contient des accesseurs de base ainsi qu'un contstructeur pour les collisions

package main.instances.enemies;

import main.utilities.Box;

import org.newdawn.slick.Graphics;

public class Enemy extends Box {
	
	public double health_max; //Points de vie maximums de l'instance
	public double health_current; //Points de vie actuels de l'instance
	
	public byte hit = 0; //Nombre de frames pendant lequel un enemy est rouge (pour indiquer un dommage). Doit etre implemente individuellement pour chaque monstre
	public final int points; //Points attribue au joueur lorsque le monstre est tue
	
	public Enemy (int x, int y, int width, int height, int hp, int points) {
		this.x = x;
		this.y = y;
		
		this.width = width;
		this.height = height;
		
		health_max = hp;
		health_current = health_max;
		
		this.points = points;
		
	}
	
	public void draw(Graphics g) {
		System.out.println("No draw   method was defined for " + this.getClass());
	}
	
	
	//Autres
	
	public int hit(int max_damage, int min_damage) { //Calcul du dommage (Override for special behaviour)
		return (int) Math.round((Math.random() * (max_damage-min_damage) + min_damage));
	}
	
	public void dimHealth(double _amount) { //Should not override, used for health diminution after previous calculations
		health_current -= _amount; //Reduit le montant de vie de l'enenmy en fonction du montant specifie
	}

	public void update(int time) {
		System.out.println("No update method was defined for " + this.getClass());
	}
}
