package main.instances.enemies;

import main.Resources;
import main.instances.EnemyBullet;
import main.states.GameState;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class Shooter extends Enemy {
	
	private int time = 500; //Temps en millisecondes (utilise pour tirer des balles a un interval fixe)
	
	public Shooter(int x, int y, int width, int height) {
		super(x, y, width, height, 15, 300);
	}
	
	@Override
	public void update (int delta) {
		y += (delta/1000.0) * 100.0;
		time += delta;
		
		if (time >= 2000) {
			GameState.bullets_enemy.add(new EnemyBullet (x + 32, y + 64));
			time -= 2000;
		}
	}

	@Override
	public void draw (Graphics g) {
		if (hit == 0)
			g.drawImage(Resources.getImage("shooter"), x, y);
		else {
			g.drawImage(Resources.getImage("shooter"), x, y, Color.red);
			hit -= 1;
		}
	}
}
