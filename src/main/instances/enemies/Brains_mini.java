//Un enemy de base utilisant le format Enemy

package main.instances.enemies;

import main.Resources;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class Brains_mini extends Enemy {

	private double initial_x;
	private double initial_y;
	private double modifier; //Pour que chaque instance ait un point de depart legerement different
	private boolean direction; //Controle du changement de direction a chaque coup
			
	public Brains_mini(int x, int y, int width, int height) {
		super(x, y, width, height, 6, 75);
		
		initial_x = x;
		initial_y = y;
		modifier = Math.random() * 100;
		direction = true;
						
		// TODO Add more behaviour aspects instead of just following a path
		// TODO Adjust path the be more enjoyable/manageable
		
		}
	
	@Override
	public void update (int delta) {
		
		y += delta/8.0; //Le point zero est vraiment important, sinon sa fourre a basse vitesse
		
		if (direction)
			x = (float) (Math.sin(Math.toRadians(y-initial_y-modifier))*150 + initial_x);
		else
			x = (float) (-(Math.sin(Math.toRadians(y-initial_y-modifier))*150) + initial_x);
	}

	@Override
	public void draw (Graphics g) {
		if (hit == 0)
			g.drawImage(Resources.getSprite("excroissance", 0, 0), x, y);
		else {
			g.drawImage(Resources.getSprite("excroissance", 0, 0), x, y, Color.red);
			hit -= 1;
		}
	}
	
	@Override
	public int hit(int max_damage, int min_damage) {
		direction = !direction;
		initial_x = x;
		initial_y = y;
		modifier = 0;
		return super.hit(max_damage, min_damage);
	}

}
