package main.instances.enemies;

import main.utilities.Box;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class Boss_Brains extends Enemy {
	
	Box hitBox;
	boolean goingLeft = false; //Pour detecter dans quelle direction se deplace le boss

	public Boss_Brains(int x, int y) {
		super(x, y, 256, 256, 50, 10000);
		
		hitBox = new Box(x + 104, y + 208, 48, 48);
		
	}

	public void update(int delta) {
		//TODO Well... It does nothing at all...
	}
	
	@Override
	public void draw (Graphics g) {
		
		g.setColor(Color.white);
		g.fillRect(x, y, width, height);
		if (hit == 0)
			g.setColor(Color.green);
		else {
			hit -= 1;
			g.setColor(Color.red);
		}
		g.fillRect(hitBox.x, hitBox.y, hitBox.width, hitBox.height);
		
		g.setColor(Color.black);
		g.drawString("HP: " + health_current, (float) x, (float) y);
	}
	
}
