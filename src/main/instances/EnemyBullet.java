package main.instances;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;

public class EnemyBullet extends PulsingBullet {
	
	public EnemyBullet (double _x, double _y) {
		super (_x,_y,10,20,0,400);
	}
	
	@Override
	public void draw (Graphics g) {
		
		//Calcul des dimensions et de la position du rectangle a dessiner (evite de faire les calculs deux fois pour deux rectangles)
		Rectangle temp_rect = new Rectangle((int) Math.round(x-(relative_width/2)),(int) Math.round(y - (relative_height/2)),(int) Math.round(relative_width),(int) Math.round(relative_height));
		
		//Dessine les deux rectangles
		g.setColor(Color.red);
		g.fill(temp_rect);		
		g.setColor(Color.black);
		g.draw(temp_rect);
		//Voila! Une balle (elle n'est pas si jolie cette fois car elle est meurtriere)
		
	}

}
