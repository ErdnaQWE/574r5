//Instance qui s'occupe de l'affichage de tout les informations sur l'ecran du joueur

package main.instances;

import main.Record;
import main.Resources;
import main.Window;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.TrueTypeFont;

public class GUI {
	
	TrueTypeFont fontMain = Resources.getFont("Mecha_Bold_64");//, 64);
	TrueTypeFont fontRecord = Resources.getFont("Mecha_Condensed_Bold_16");//, 16);
	
	public void draw(Graphics g, int score, float health_percent, float energy_percent) {		
		/*		 
		 -experience?
		 -Idee de Patry (Bordure autour du jeu au complet, sa donnerait une effet cool)
		 	-Une autre idee serait de juste mettre une bordure (style cinematique mais pas vraiment) pendant les boss fights pour indiquer que le vaisseau ne bouge plus/ tu est pris...		 
		 */
		
		g.setColor(Color.black);
		g.fillRect(0, Window.HEIGHT - 104, Window.WIDTH, Window.HEIGHT); //Dessine un rectangle noir afin de cacher tout les ennemis qui s'aventurent en dessous
		
		g.setColor(Color.white);
		
		g.setFont(fontMain);
		//La ligne suivante cause des problemes quand je fait le fichier jar
		g.drawString(score + "pts", (Window.WIDTH/2) - (fontMain.getWidth(score + "pts")/2) , Window.HEIGHT - fontMain.getHeight(score  + "pts") - 16); //Dessine les points actuels du joueur dans le milieu horizontal au bas de l'ecran
		//g.drawString(score + "pts", 140 , 710); //Dessine les points actuels du joueur dans le milieu horizontal au bas de l'ecran

		g.setFont(fontRecord);
		//g.drawString("Record: " + Record.get() + "pts",	140, 780); //Dessine le record actuel
		g.drawString("Record: " + Record.get() + "pts", (Window.WIDTH/2) - (fontRecord.getWidth("Record: " + Record.get() + "pts")/2) , Window.HEIGHT - fontRecord.getHeight("Record: " + Record.get())); //Dessine le record actuel
		
		for (int i = 100; i < 105; i++) //Dessine 5 lignes (pour faire une ligne plus epaisse) afin de separer le jeu de l'interface
			g.drawLine(0,Window.HEIGHT - i, Window.WIDTH, Window.HEIGHT - i);
		
		//Dessine les orbes de vie et energie (ainsi que des orbes vides afin de simuler un dome en vitre qui se vide graduellement)
		g.drawImage(Resources.getImage("orb_vide"), 0, Window.HEIGHT - 128);
		g.drawImage(Resources.getImage("orb_vie"), 0, Window.HEIGHT - (128 * health_percent), 128, Window.HEIGHT, 0, 128 - (128 * health_percent), 128, 128);
		g.drawImage(Resources.getImage("orb_vide_inverse"), Window.WIDTH - 128, Window.HEIGHT - 128);
		g.drawImage(Resources.getImage("orb_energie"), Window.WIDTH - 128, Window.HEIGHT - (128 * energy_percent), Window.WIDTH, Window.HEIGHT, 0, 128 - (128 * energy_percent), 128, 128);
		
	}
	
}
