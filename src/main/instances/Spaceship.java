package main.instances;

import main.Resources;
import main.utilities.Box;

import org.newdawn.slick.Graphics;

public class Spaceship extends Box {
	
	private int time = 0; //Temps (pour l'animation)
	
	public Spaceship (int x, int y) {
		this.x = x;
		this.y = y;
		
		this.width = 64;
		this.height = 64;
	}


	public void update(int delta) {
		// TODO Position calculations
		//Maybe add the input as parameters
		
		time += delta;
		
		if (time >= 300)
			time = time % 300; //Enleve tout les multiples de 300
	}

	
	public void draw(Graphics g) {
		
		g.drawImage(Resources.getSprite("spaceship",(int) Math.floor(time/75.0), 0), x, y);
		
		/*
		g.setColor(Color.white);
		//Forme de spaceship temporaire
		g.drawLine((float) x-32, (float) y+32, (float) x,    (float) y-32);
		g.drawLine((float) x,    (float) y-32, (float) x+32, (float) y+32);
		g.drawLine((float) x-32, (float) y+32, (float) x,    (float) y+16);
		g.drawLine((float) x,    (float) y+16, (float) x+32, (float) y+32);
		//g.drawLine((float) x,    (float) y,    (float) x,    (float) y   ); //Position DEBUG
		*/
	}
	
	public void incX(double _x) { //TODO Obsolete?
		x += _x;
	}
	
	public void incY(double _y) { //TODO Obsolete?
		y += _y;
	}

}