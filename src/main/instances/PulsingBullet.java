//Une balle style cartoon (qui a une apparence de pulsation)
//Comprend une methode pour dessiner avec Slick2D
//La balle se deplace directement vers le haut par defaut
//Il est possible de changer de direction avec le parametre d'angle
//L'angle doit etre specifie en degree, il sera convertit en radians dans le code.
//0 degre au haut, 90 a la gauche, 180 le bas, etc.
//C'est a noter que l'image de la balle ne subit pas de rotation, il est donc preferable de choisir une balle carre pour un angle qui n'est pas un multiple de 90


package main.instances;

import main.utilities.Box;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;

public class PulsingBullet extends Box {
	
	protected double relative_width; //Dimensions apres la dernier mise a jour (permet l'effet de pulsation)
	protected double relative_height;
	
	//Vitesse horizontale et verticale (par seconde) de la balle
	protected double v_speed = 0; //La vitesse devrait etre negative afin que la balle se deplace vers le haut
	protected double h_speed = 0;
	
	protected int timeLived = 0; //Donne a la balle une perception du temps (utilise pour l'effet de pulsation)
	
	public PulsingBullet (double x, double y) { //Balle normale qui se deplace vers le haut
		this (x, y, 20, 10, 0, -400);
	}
	
	public PulsingBullet (double x, double y, int width, int height) { //Balle d'une taille differente
		this (x, y, width, height, 0, -400);
	}
	
	public PulsingBullet (double x, double y, int width, int height, int speed) { //Balle ayant une vitesse differente
		this (x, y, width, height, speed, -400);
	}
	
	public PulsingBullet (double x, double y, int width, int height, int angledeg, int speed) { //Balle ayant un angle differente (et donc qui se deplace dans une differente direction)
		this.x = (float) x;
		this.y = (float) y;
		this.width = width;
		this.height = height;

		//Calcul de la direction de la balle a partir de l'angle (en degre) specifie
		if (angledeg % 360 != 0 ) { //Si l'angle n'est pas un multiple de 360 (donc ne va pas directement vers le haut)
			v_speed = speed * Math.cos(Math.toRadians(angledeg));
			h_speed = speed * Math.sin(Math.toRadians(angledeg));
		}
		else
			v_speed = speed;
		
		relative_width = width;
		relative_height = height;
		
	}
	
	
	public void draw (Graphics g) {
		
		//Calcul des dimensions et de la position du rectangle a dessiner (evite de faire les calculs deux fois pour deux rectangles)
		Rectangle temp_rect = new Rectangle((int) Math.round(x-(relative_width/2)),(int) Math.round(y - (relative_height/2)),(int) Math.round(relative_width),(int) Math.round(relative_height));
		
		//Dessine les deux rectangles
		g.setColor(Color.white);
		g.fill(temp_rect);		
		g.setColor(Color.black);
		g.draw(temp_rect);
		//Voila! Une jolie petite balle 	
	}
	
	public void update (int delta) {
		//Positioning update
		x += h_speed * (delta / 1000.0);
		y += v_speed * (delta / 1000.0);
		
		//Size update
		timeLived += delta;
		relative_height = height * (Math.sin(6 * (timeLived / 1000.0)));
		relative_width = width * (Math.sin(6 * (timeLived / 1000.0)));
		
		if (timeLived > 10000) timeLived-=(Math.PI*2)*1000; //Removes whole periods from the counted time
	}
	
}
