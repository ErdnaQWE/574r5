package main;

import main.levels.World;
import main.states.*;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public class Engine extends StateBasedGame {
	
	public Engine() {
		super("574r5"); //Window Title
	}

	public static void main(String[] args) {
		try {
			AppGameContainer game = new AppGameContainer(new Engine()); //Cree le conteneur de jeu
			game.setDisplayMode(Window.WIDTH, Window.HEIGHT, Window.FULLSCREEN); //Initialise la taille de la fenetre de jeu
			game.start(); //Demarre le jeu
		} catch (SlickException e) {
			e.printStackTrace();
			System.out.println("Game could not be initialised at main method");
			System.out.println("Usually, this is an error in coding");
		}
	}

	@Override
	public void initStatesList(GameContainer gc) throws SlickException {
		gc.setTargetFrameRate(60); //Place la cible d'images par secondes a 60
		gc.setMaximumLogicUpdateInterval(60);
		//gc.setMinimumLogicUpdateInterval(30);
		gc.setVSync(true); //Active VSync
		gc.setAlwaysRender(true); //Force le jeu a dessiner meme si le jeu n'est pas en focus
		gc.setShowFPS(false); //Eneleve le compteur d'images par secondes par defaut (un compteur personalise est rajoute au besoin dans chaque state)
		
		new Resources(); // Chargement de tout les resources du jeux
		
		try {
			World.load("area.json");
		} catch (Exception e) {
			System.err.println("Map does not exist :-(");
			e.printStackTrace();
			System.exit(0);
		}
		
		//this.addState(new TestState());
		//this.addState(new SkillState());
		this.addState(new IntroState());
		this.addState(new MenuState());
		this.addState(new GameState());
		this.addState(new OverState());
		
	}

}
