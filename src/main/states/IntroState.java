package main.states;

import main.Resources;
import main.Window;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class IntroState extends BasicGameState {

	private int time = 0; //Temps en millisecondes, afin de savoir comment longtemps afficher un logo et quand passer a la prochaine state
	
	int[][] stars_s = new int[75][2];
	int[][] stars_m = new int[25][2];
	int[][] stars_l = new int[10][2];
	
	TrueTypeFont mainFont = Resources.getFont("Mecha_Condensed_32");
	
	
	@Override
	public void init(GameContainer gc, StateBasedGame s) throws SlickException {
		
		for (int i = 0; i < 75; i++) { //Petites
			stars_s[i][0] = (int) (Math.random()*Window.WIDTH);
			stars_s[i][1] = (int) (Math.random()*Window.HEIGHT);
		}
		
		for (int i = 0; i < 25; i++) { //Moyennes
			stars_m[i][0] = (int) (Math.random()*Window.WIDTH);
			stars_m[i][1] = (int) (Math.random()*Window.HEIGHT);
		}
		
		for (int i = 0; i < 10; i++) { //Larges
			stars_l[i][0] = (int) (Math.random()*Window.WIDTH);
			stars_l[i][1] = (int) (Math.random()*Window.HEIGHT);
		}
		
	}

	@Override
	public void render(GameContainer gc, StateBasedGame s, Graphics g) throws SlickException {
		
		//g.drawString("time: " + time, 0,0); //DEBUG
		
		if (time>5000) {
			g.setColor(new Color(0,0,32));
			g.fillRect(0, 0, Window.WIDTH, Window.HEIGHT);
			g.setColor(new Color(255,255,179));
			for (int i = 0; i < 75; i++) //TODO Play with the size of the stars
				g.fillRect(stars_s[i][0],(float) (stars_s[i][1] + ((time/62.5))) % Window.HEIGHT, 1, 1);
		
			for (int i = 0; i < 25; i++)
				g.fillRect(stars_m[i][0],(float) (stars_m[i][1] + ((time/31.25))) % Window.HEIGHT, 5, 5);
		
			for (int i = 0; i < 10; i++)
				g.fillRect(stars_l[i][0],(float) (stars_l[i][1] + ((time/15.625))) % Window.HEIGHT, 10, 10);
		}
		
		
		if (time<=1500) {
			Image temp = Resources.getSprite("dion_anim", (int) Math.round((time/1500.0) * 12.0), 0);
			g.drawImage(temp, Window.WIDTH/2 - temp.getWidth()/2 + 123, Window.HEIGHT/2 - temp.getHeight()/2 - 114);
		}
		else if (time<=2500) 
			g.drawImage(Resources.getImage("dion2"), Window.WIDTH/2 - Resources.getImage("dion2").getWidth()/2,Window.HEIGHT/2 - Resources.getImage("dion2").getHeight()/2 - 150);
		else if (time<=3500) {
			g.drawImage(Resources.getImage("dion2"), Window.WIDTH/2 - Resources.getImage("dion2").getWidth()/2,Window.HEIGHT/2 - Resources.getImage("dion2").getHeight()/2 - 150, new Color(1,1,1,(float) (1.0-(time-2500.0)/1000.0)));
			g.drawImage(Resources.getImage("dion"), Window.WIDTH/2 - Resources.getImage("dion").getWidth()/2,Window.HEIGHT/2 - Resources.getImage("dion").getHeight()/2 - 150,new Color(1,1,1,(float) ((time-2500.0)/1000.0)));
		}
		else if (time<=5000)
			g.drawImage(Resources.getImage("dion"), Window.WIDTH/2 - Resources.getImage("dion").getWidth()/2,Window.HEIGHT/2 - Resources.getImage("dion").getHeight()/2 - 150);
		else if (time<=10000) {
			g.setColor(new Color(0,0,0,(float) (1.0-(time-5000.0)/5000.0)));
			g.fillRect(0,0,Window.WIDTH,Window.HEIGHT);
			g.drawImage(Resources.getImage("dion"), Window.WIDTH/2 - Resources.getImage("dion").getWidth()/2,Window.HEIGHT/2 - Resources.getImage("dion2").getHeight()/2 - 150, new Color(1,1,1,(float) (1.0-(time-5000.0)/5000.0)));
		}
		
	}

	@Override
	public void update(GameContainer gc, StateBasedGame s, int delta) throws SlickException {
		time += delta;
		
		if (time > 12000 || gc.getInput().isKeyPressed(Input.KEY_SPACE))
			s.enterState(States.MENU);
		
	}

	@Override
	public int getID() {
		return States.INTRO; //Numero d'identification
	} 

}
