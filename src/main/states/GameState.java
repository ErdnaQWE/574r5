package main.states;

import java.util.ArrayList;

import main.*;
import main.instances.*;
import main.instances.enemies.*;
import main.levels.*;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

public class GameState extends BasicGameState {
		
	// Objets et listes
	ArrayList<PulsingBullet> bullets = new ArrayList<PulsingBullet>(); //Liste d'objets contenant tout les instances de balles (du joueur)
	public static ArrayList<PulsingBullet> bullets_enemy = new ArrayList<PulsingBullet>(); //Liste d'objets contenant tout les instances de balles (des monstres)
	public static ArrayList<Enemy> instances = new ArrayList<Enemy>(); //Liste d'objets contenant tout les enemies
	Spaceship spaceship = new Spaceship(300,600); //Le vaisseau du joueur principal
	GUI gui = new GUI(); //L'interface de jeu (barre de vies, points)
	Level level = new LArcade(); //Niveau en cours
	
	//Variables
	//Techniques
	int[][] starsS = new int[75][2];
	int[][] starsM = new int[25][2];
	int[][] starsL = new int[10][2];
	boolean showFPS = false; //Toggle pour afficher et cacher le FPS
	
	//Jeu
	Info info = new Info();
	
	float hpMax = 10; //Vie maximales du joueur
	float hpCurrent = hpMax; //Vie actuels du joueur
	float energyMax = 3; //Energie maximal
	float energyCurrent = energyMax; //Eenrgie actuelle du joueur
	//int max_damage = 4; //Dommages maximums
	//int min_damage = 2; //Dommages minimums
	int score = 0; //Points actuels du joueur
	int drawScore = 0; //Points a afficher sur l'ecran, pour l'effet compteur
	long timeStars = 0; //Temps en millisecondes depuis le debut du niveau pour l'animation des etoiles
	boolean leftCannon = true; //Afin de pouvoir detecter si la balle devrait sortir du cannon gauche ou droite (true = gauche)
		
	//Autres
	TrueTypeFont mainFont = Resources.getFont("Mecha_Condensed_Bold");//, 16f); //Font utilise pour l'affichage des points (temporaire)
	
	
	
	
	
	@Override
	public void init(GameContainer gc, StateBasedGame s) throws SlickException {
		
		Record.init(); //Initialise le record (ne fait que le charger du fichier texte)
		
		
		//Generation de la position des etoiles
		for (int i = 0; i < 75; i++) { //Petites
			starsS[i][0] = (int) (Math.random()*Window.WIDTH);
			starsS[i][1] = (int) (Math.random()*Window.HEIGHT);
		}
		
		for (int i = 0; i < 25; i++) { //Moyennes
			starsM[i][0] = (int) (Math.random()*Window.WIDTH);
			starsM[i][1] = (int) (Math.random()*Window.HEIGHT);
		}
		
		for (int i = 0; i < 10; i++) { //Larges
			starsL[i][0] = (int) (Math.random()*Window.WIDTH);
			starsL[i][1] = (int) (Math.random()*Window.HEIGHT);
		}
	}

	
	
	
	
	@Override
	public void render(GameContainer gc, StateBasedGame s, Graphics g) throws SlickException {
		
		//World
		
		//World.render(0, 0);

		
		//Arriere-plan 
		
		g.setColor(new Color(0,0,32));
		g.fillRect(0, 0, Window.WIDTH, Window.HEIGHT);
		g.setColor(new Color(255,255,179));
		for (int i = 0; i < 75; i++)
			g.fillRect(starsS[i][0],(float) (starsS[i][1] + ((timeStars/62.5))) % Window.HEIGHT, 1, 1);
		
		for (int i = 0; i < 25; i++)
			g.fillRect(starsM[i][0],(float) (starsM[i][1] + ((timeStars/31.25))) % Window.HEIGHT, 3, 3);
		
		for (int i = 0; i < 10; i++)
			g.fillRect(starsL[i][0],(float) (starsL[i][1] + ((timeStars/15.625))) % Window.HEIGHT, 5, 5);
		
		
		//Dessin des balles (une par une)
		for (int i = 0; i < bullets.size(); i++) {
			PulsingBullet element = bullets.get(i); //L'objet en cours de traitement
			element.draw(g); //Execute la methode draw de chaque instance (ce qui permetterait d'avoir differents types de bullets avec des apparences differentes)
		}
		
		
		//Dessine tout les instances/monstres
		for (int i = 0; i < instances.size(); i++) {
			Enemy element = instances.get(i); //L'objet en cours de traitement
			element.draw(g);
		}
		
		
		//Dessin des balles enemies
		for (int i = 0; i < bullets_enemy.size(); i++) {
			PulsingBullet element = bullets_enemy.get(i); //L'objet en cours de traitement
			element.draw(g); //Execute la methode draw de chaque instance (ce qui permetterait d'avoir differents types de bullets avec des apparences differentes)
		}
		
		
		spaceship.draw(g); //Dessine le vaisseau spatial du joueur
		
		if (showFPS) {
			g.setFont(mainFont);
			g.setColor(Color.white);
			g.drawString("574r5 | ALPHA", 17, 17);
			g.drawString(gc.getFPS() + " fps", 17, 31);
			g.drawString(instances.size() + " instances", 17, 45);
			g.drawString(bullets.size() + " + " + bullets_enemy.size() + " bullets", 17, 59);
		}
		
		gui.draw(g, drawScore, hpCurrent / hpMax, energyCurrent / energyMax); //Dessine l'interface (en lui passant quelques variables de jeu) L'interface devrait toujours etre dessine en dernier afin qu'il se retrouve au dessus de tout autre image
		
	}

	
	@Override
	public void update(GameContainer gc, StateBasedGame s, int delta) throws SlickException {
		
		timeStars += delta;
		
		double spaceship_speed = (delta / 1000.0) * 400;
		
		//Mise a jour du vaisseau
		//Input (pour le vaisseau seulment
		if (gc.getInput().isKeyDown(Input.KEY_LEFT)) {
			spaceship.incX(-(spaceship_speed));
			if (spaceship.x < 0) spaceship.x = 0;
		}
		else if (gc.getInput().isKeyDown(Input.KEY_RIGHT)) {
			spaceship.incX(spaceship_speed);
			if (spaceship.x > Window.WIDTH - spaceship.width) spaceship.x = Window.WIDTH - spaceship.width;
		}
		
		if (gc.getInput().isKeyDown(Input.KEY_UP)) {
			spaceship.incY(-(spaceship_speed));
			if (spaceship.y < 0) spaceship.y = 0;
		}
		else if (gc.getInput().isKeyDown(Input.KEY_DOWN)) {
			spaceship.incY(spaceship_speed);
			if (spaceship.y > Window.HEIGHT - 137) spaceship.y = Window.HEIGHT-137;
		}
		
		spaceship.update(delta); //Mise a jour de la position du vaisseau
		
		//Autre input
		//Attaques
		if (gc.getInput().isKeyPressed(Input.KEY_SPACE) || gc.getInput().isKeyPressed(Input.KEY_LCONTROL)) {
			if (leftCannon)
				bullets.add(new PulsingBullet (spaceship.x + 10, spaceship.y, 10, 20, 0, -400)); //Tire une balle du cannon gauche
			else
				bullets.add(new PulsingBullet (spaceship.x + 56, spaceship.y, 10, 20, 0, -400)); //Tire une balle du cannon droit //TODO Verify good position
			
			leftCannon = !leftCannon;
		}
		
		if (gc.getInput().isKeyPressed(Input.KEY_B) && energyCurrent >= 1) { //Tire 19 balles carres dans en position de demi cercle
			energyCurrent -= 1;
			for (int i = -90; i < 91; i+=10) {
				bullets.add(new PulsingBullet (spaceship.x, spaceship.y - 32,10,10,i,-400));
			}
		}

		//DEBUG (Invoque des monstres pour les tester)
		if (gc.getInput().isKeyPressed(Input.KEY_Q))
			instances.add(new Brains_mini(300, -32, 32, 32));
		if (gc.getInput().isKeyPressed(Input.KEY_W))
			instances.add(new Invader(300, 0, 64, 64));
		if (gc.getInput().isKeyPressed(Input.KEY_E))
			instances.add(new Shooter(300, -36, 64, 64));
		if (gc.getInput().isKeyPressed(Input.KEY_R))
			instances.add(new Speedster(300, -36, 64, 64));
		
		if (gc.getInput().isKeyPressed(Input.KEY_M))
			s.enterState(States.MENU); //Retourne au menu
		
		if (gc.getInput().isKeyPressed(Input.KEY_F))
			showFPS = !showFPS; //Toggle le FPS
			
		//Mise a jour du niveau
		level.update(delta);
		
		//Update des objets
		for (int i = 0; i < bullets.size(); i++) {
			PulsingBullet element = bullets.get(i); //L'objet en cours de traitement
			element.update(delta); //Mise a jour de la position des balles
			
			//Si la balle sort de l'ecran
			if (element.y < -element.height || element.y > Window.HEIGHT + element.height || element.x > Window.WIDTH + element.width || element.x < -element.width)
				bullets.remove(i); //On l'enleve de la liste (et de la memoire)
		}
		
		for (int i = 0; i < bullets_enemy.size(); i++) {
				PulsingBullet element = bullets_enemy.get(i); //L'objet en cours de traitement
				element.update(delta);
				
				if (element.y < -element.height || element.y > Window.HEIGHT + element.height || element.x > Window.WIDTH + element.width || element.x < -element.width)
					bullets_enemy.remove(i); //On l'enleve de la liste (et de la memoire)
				
		}
		
		//Mise a jour des variables
		if (energyCurrent > energyMax) 	
			energyCurrent = energyMax; //S'assure que l'energie ne depasse pas la limite
		else if (energyCurrent < energyMax)
			energyCurrent += delta/5000.0; //Augmente l'energie lentement si elle n'est pas au maximum
		
		//Mise a jour des monstres (Le comportement)
		for (int i = 0; i < instances.size(); i++) {
			Enemy element = instances.get(i);
			element.update(delta);
			
			//Fait disparaitre le monstre si il sort de l'ecran
			if (element.y > Window.HEIGHT + element.height + 64) {
    				instances.remove(i);
    				i -= 1; //Reduit les montant de verifications de 1 afin d'eviter de sortir du range de la liste
			}	
		}
		
		
		//Collision detection
		{ //Balles du joueur contre enemies
			int _i = instances.size();
			int _ii = bullets.size();
			for (int i = 0; i < _i; i++) {
				Enemy element = instances.get(i); //L'instance en cours de traitement
				for (int ii = 0; ii < _ii; ii++) {
					PulsingBullet element2 = bullets.get(ii);
	        		if (element.hitTest(element2)) {
	        			bullets.remove(ii);
	        			_ii -= 1;
	        			ii -= 1;
	        			element.dimHealth(element.hit(info.max_damage, info.min_damage));
	        			element.hit = 3;
	        			
	        			if (element.health_current <= 0) {
	        				score += element.points;
	        				instances.remove(i);
	        				_i -= 1;
	        				i -= 1;
	        				break;
	        			}
	        		}
				}
			}
		}
		
		{ //Enemies contre le vaisseau du joueur
			for (int i = 0; i < instances.size(); i++) {
				Enemy element = instances.get(i); //L'instance en cours de traitement
			
				if (element.hitTest(spaceship)) { //Verifie pour une collision entre le joueur et un monstre
					hpCurrent -= 2;
					
					if (hpCurrent <= 0) {
    					if (Record.check(score)) Record.set(score);
    					s.enterState(States.OVER, new FadeOutTransition(), new FadeInTransition());
					}
					
					instances.remove(i);
					i -= 1;
    				
				}
			}
			
		}
		
		{ //Balles des enemies contre le vaisseau du joueur
			for (int i = 0; i < bullets_enemy.size(); i++) {
				PulsingBullet element = bullets_enemy.get(i); //L'instance en cours de traitement
				
				if (element.hitTest(spaceship)) { //Verifie pour une collision entre le joueur et un monstre
					hpCurrent -= 1;
						
					if (hpCurrent <= 0) {
    					if (Record.check(score)) Record.set(score);
	    				s.enterState(States.OVER, new FadeOutTransition(), new FadeInTransition());
					}
						
					bullets_enemy.remove(i);
					i -= 1;
				}
			}
		}
		
		
		//Other calculations
		int score_diff = (int) ((score - drawScore) * 2.0 * (delta/1000.0)); //Calcule le nombre de points a rajouter au compteur
		if (score_diff > 1) drawScore += score_diff; //Verifie si les points a ajouter au compteur sont superieure a un (pour eviter que le comtpeur reste pris lorsque la difference devient trop petite.
		else drawScore = score; //Si la difference est trop petite, ajuste le compteur de points pour afficher le montant exact.
		
		
		
	}

	@Override
	public int getID() {
		return States.GAME; //Numero d'identification
	}

}