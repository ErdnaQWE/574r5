package main.states;

import main.Resources;
import main.Window;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class OverState extends BasicGameState {
	
	TrueTypeFont fontTitle = Resources.getFont("Mecha_Bold_64");//, 64f);
	TrueTypeFont fontMain = Resources.getFont("Mecha_Condensed_32");//, 32f);

	@Override
	public void init(GameContainer gc, StateBasedGame s) throws SlickException {
		//Auto-generated method stub
	}

	@Override
	public void render(GameContainer gc, StateBasedGame s, Graphics g) throws SlickException {
		g.setFont(fontTitle);
		g.setColor(Color.white);
		g.drawString("Fin de la partie", Window.WIDTH/2 - fontTitle.getWidth("Fin de la partie")/2, 200);
		
		g.setFont(fontMain);
		g.drawString("On se rappelera de vous", Window.WIDTH/2 - fontMain.getWidth("On se rappelera de vous")/2, 300);
		g.drawString("a travers le temps", Window.WIDTH/2 - fontMain.getWidth("a travers le temps")/2, 332);
		g.drawString("... et l'espace.", Window.WIDTH/2 - fontMain.getWidth("... et l'espace.")/2, 364);
		
		g.drawString("Merci d'avoir jouer!", Window.WIDTH/2 - fontMain.getWidth("Merci d'avoir jouer!")/2, 428);
		g.drawString("                -Dion", Window.WIDTH/2 - fontMain.getWidth("                -Dion")/2, 460);

	}

	@Override
	public void update(GameContainer gc, StateBasedGame s, int delta) throws SlickException {
		// TODO Actually make some gameover screen
		
	}

	@Override
	public int getID() {
		return States.OVER;
	}

}
