package main.states;

import main.Resources;
import main.Window;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class TestState extends BasicGameState {
	
	TrueTypeFont fontTitle = Resources.getFont("Mecha_Bold_64");
	TrueTypeFont fontMain = Resources.getFont("Mecha_Condensed_Bold_48");
	TrueTypeFont fontScore = Resources.getFont("Mecha_Condensed_32");
	
	int time = 0; //Temps pour les animations
	

	@Override
	public void init(GameContainer gc, StateBasedGame s) throws SlickException {
		//Auto-generated method stub
	}

	@Override
	public void render(GameContainer gc, StateBasedGame s, Graphics g) throws SlickException {
		
		g.drawString("play t = " + time, 0, 0); //DEBUG (Pour savoir quand les animations commencents
		
		//Apparition du titre du niveau
		if (time <= 300) {
			g.setFont(fontTitle);
			g.drawString("Area 0x09", Window.WIDTH - ((Window.WIDTH/2) + (fontTitle.getWidth("Area 0x09")/2)) * (time/300f), 75);
		}
		else {
			g.setFont(fontTitle);
			g.drawString("Area 0x09", Window.WIDTH/2 - fontTitle.getWidth("Area 0x09")/2, 75);
		}
		
		//Apparition des points
		if (time > 300 && time <= 600) {
			g.setFont(fontMain);
			g.drawString("Score", Window.WIDTH - ((Window.WIDTH/2) + (fontMain.getWidth("Score")/2)) * ((time-300)/300f), 200);
			g.setFont(fontScore);
			g.drawString("0", Window.WIDTH - ((Window.WIDTH/2) + (fontScore.getWidth("0")/2)) * ((time-300)/300f), 250);
		}
		else if (time > 600 && time <= 1600) {
			g.setFont(fontMain);
			g.drawString("Score", Window.WIDTH/2 - fontMain.getWidth("Score")/2, 200);
			g.setFont(fontScore);
			int score = (int) (123456 * ((time-600)/1000f));
			g.drawString(Integer.toString(score), Window.WIDTH/2 - fontScore.getWidth(Integer.toString(score))/2, 250);
			}
		else if (time > 1600) {
			g.setFont(fontMain);
			g.drawString("Score", Window.WIDTH/2 - fontMain.getWidth("Score")/2, 200);
			g.setFont(fontScore);
			g.drawString("123456", Window.WIDTH/2 - fontScore.getWidth("123456")/2, 250);
		}
		
		//Apparition des monstres tuee (C'est la partie amusante ici)
		//Utilisera du temps delta, So de 1600 a 1700 moins 1000, loop augmente de 1, recommence

		if (time > 1600) {
			//Fait apparaitre un monstre (Slide in de la gauche)
			
			
			//Compteur monte pour le montant tue
			//Compteur monte pour les points
			//Tasse a la droite, le prochain apparait le tout recommence.
		}
		
		
	}

	@Override
	public void update(GameContainer gc, StateBasedGame s, int delta) throws SlickException {
		time+=delta;
	}

	@Override
	public int getID() {
		return States.TEST;
	}

}
