package main.states;

import main.Resources;
import main.Window;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class MenuState extends BasicGameState{
	
	TrueTypeFont fontTitle = Resources.getFont("Mecha_Bold_64");//, 64f);
	TrueTypeFont fontMain = Resources.getFont("Mecha_Condensed_32");// 32f);
	
	double x = 200; //Positionement du curseur
	double y = 1000;
	
	double target_y = Window.HEIGHT/2 - 100; //Cible du curseur
	
	int position = 1; //Probablement changer ceci a byte, c'est la position dans les options du menu donc 1 = jouer, 2 = options, etc.

	@Override
	public void init(GameContainer gc, StateBasedGame s) throws SlickException {
		//Auto-generated method stub
	}

	@Override
	public void render(GameContainer gc, StateBasedGame s, Graphics g) throws SlickException {
		/*Debug Info
		g.drawString("574r5 | MENU | FPS: " + gc.getFPS(), 0, 0);
		g.drawString("targetY: " + target_y + "| Y: " + y, 0, 12);
		g.drawString("Position: " + position, 0, 24);
		*/
		
		//Dessiner le curseur/selecteur
		g.setColor(new Color(255, 255, 255, 0.25f));
		g.fillRect((float) Window.WIDTH/2 - 100, (float) y - 5, (float) 200, (float) fontMain.getHeight() + 10);
		g.setColor(Color.white);
		g.drawRect((float) Window.WIDTH/2 - 100, (float) y - 5, (float) 200, (float) fontMain.getHeight() + 10);
		
		//Dessiner le texte
		g.setColor(Color.white);
		g.setFont(fontTitle);
		g.drawString("574r5", Window.WIDTH/2 - fontTitle.getWidth("574r5")/2, 200);
		
		g.setFont(fontMain);
		g.drawString("Jouer", Window.WIDTH/2 - fontMain.getWidth("Jouer")/2, Window.HEIGHT/2 - 100);
		g.drawString("Controles", Window.WIDTH/2 - fontMain.getWidth("Controles")/2, Window.HEIGHT/2 - 50);
		g.drawString("Options", Window.WIDTH/2 - fontMain.getWidth("Options")/2, Window.HEIGHT/2);
		g.drawString("Quitter", Window.WIDTH/2 - fontMain.getWidth("Quitter")/2, Window.HEIGHT/2 + 50);
	}

	@Override
	public void update(GameContainer gc, StateBasedGame s, int delta) throws SlickException {
		
		if ((gc.getInput().isKeyPressed(Input.KEY_UP)) && (position > 1)) {
			target_y -= 50;
			position -= 1;
		}
		else if ((gc.getInput().isKeyPressed(Input.KEY_DOWN)) && (position < 4)) {
			target_y += 50;
			position += 1;
		}
		
		if (gc.getInput().isKeyPressed(Input.KEY_SPACE)) {
			switch (position) {
				case 1: //Jouer
					s.enterState(States.GAME);
					break;
				case 2: //Controles
					// TODO Rien pour tout de suite
					break;
				case 3: //Options
					// TODO Rien pour tout de suite
					break;
				case 4: //Quitter
					gc.exit();
					break;
			}
		}
 

		//Mise a jour de la position du curseur
		if (target_y > y)
			y += (target_y - y) * 0.15;
		else
			y -= (y - target_y) * 0.15;
		
		
	}

	@Override
	public int getID() {
		return States.MENU; //Numero d'identification
	}

}
