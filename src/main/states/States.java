package main.states;

public class States {

	public static final int INTRO = 0; //Introduction au debut du jeu avec tout les logos (il va probablement juste en avoir un). Ce qui serait possible de faire serait un petit intro video comme dans la plupart des jeux)
	public static final int MENU = 1; //Menu Principal
	public static final int GAME = 2; //Jeu Principal (c'est ici qu'on retrouve la plupart de l'action)
	public static final int OVER = 3; //Ecran GameOver/Fin de la partie (Le but est d'eviter de se render a cet ecran)
	public static final int TEST = 4; //Salle de test pour divers codes
	public static final int SKILL = 10;
	//public static final int CINEMATIC = -1; //Petite animation au debut d"une partie qui raconte une partie de l"histoire
}
