package main.states;

import main.Resources;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class SkillState extends BasicGameState{
	
	TrueTypeFont fontTitle = Resources.getFont("Mecha_Bold_64");
	TrueTypeFont fontMain = Resources.getFont("Mecha_Condensed_32");
	
	float x = 100; //Positionement de la camera
	float y = 100;
	float h_v = 0; //Velocite horizontale
	float v_v = 0; //Velocite verticale

	@Override
	public void init(GameContainer gc, StateBasedGame s) throws SlickException {
		//Auto-generated method stub
	}

	@Override
	public void render(GameContainer gc, StateBasedGame s, Graphics g) throws SlickException {
		//g.drawString("X: " + x + " | Y: " + y + " | h_v: " + h_v + " | v_v: " + v_v, 0, 0); //DEBUG
		
		g.drawLine(x+32, y+32, x+128+32, y+128+32);
		g.fillRect(x, y, 64, 64);
		g.fillRect(x+128, y+128, 64, 64);
	}

	@Override
	public void update(GameContainer gc, StateBasedGame s, int delta) throws SlickException {
		
		double delta_s = delta / 1000.0; //Evite de refaire le calcul un million de fois
		
		if (gc.getInput().isKeyDown(Input.KEY_LEFT)) {
			if (h_v >= -200) {
				h_v -= 200.0 * delta_s;
			}
			else h_v = -200;
		}
		else if (gc.getInput().isKeyDown(Input.KEY_RIGHT)) {
			if (h_v <= 200) {
				h_v += 200.0 * delta_s;
			}
			else h_v = 200;
		}
		
		if (gc.getInput().isKeyDown(Input.KEY_UP)) {
			if (v_v >= -200) {
				v_v -= 200.0 * delta_s;
			}
			else v_v = -200;
		}
		else if (gc.getInput().isKeyDown(Input.KEY_DOWN)) {
			if (v_v <= 200) {
				v_v += 200.0 * delta_s;
			}
			else v_v = 200;
		}
		
		if (h_v > 2 || h_v < -2)
			x += h_v * delta_s;
		if (v_v > 2 || v_v < -2)
			y += v_v * delta_s;
		
		if (h_v != 0)
			h_v -= h_v * delta_s;
		if (v_v != 0)
			v_v -= v_v * delta_s;
	}
	
	@Override
	public int getID() {
		return States.SKILL; //Numero d'identification
	}

}
