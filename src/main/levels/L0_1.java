//Utilise pour tester les systeme
//Exemple d'un niveau typique

package main.levels;

public class L0_1 extends Level {
	
	public String name = "L0_1"; //Nom du niveau (Si il faut afficher au dessus des points ou au debut du niveau) TODO
	
	@Override
	public void update(int delta) { //Cette fonction sera appele par l'Update loop de GameState
		
		time += delta; //Temps depuis le dernier update
		
		while (time >= next) { //Detecte si c'est le temps d'un evenement. Utilise une boucle while pour spawner tout les evenements si le jeu lag hard!!!
			
			System.out.println("Time is " + time);
			time -= next; //Si oui, enleve le temps attendu 
		
			switch (next_num) {
			
			case 0:
				summon(1,300,-50);
				next = 5000;
				break;
			case 1:
				summon(1,150,-50);
				summon(1,450,-200);
				next = 5000;
				break;
			case 2:
				summon(1,150,-50);
				summon(1,450,-100);
				summon(1,300,-150);
				summon(1,200,-200);
				summon(1,400,-250);
				next = 0;
			case 3:
				System.out.println("Level over! Reminder in 60 seconds");
				next = 60000; 
				next_num-=1;
				break;
	
			}
			
			next_num+=1;
		
		}
		
	}

}
