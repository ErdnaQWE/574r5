package main.levels;

import java.io.InputStreamReader;

import main.Resources;
import main.Window;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.newdawn.slick.Image;
import org.newdawn.slick.SpriteSheet;

public class World {

	public static Image[][] environment;
	public static int WIDTH;
	public static int HEIGHT;
	
	public static void render(float xRender, float yRender) {
		
		int offset = 2;
		int xStart = (int) (xRender / Tile.SIZE) - offset;
		int yStart = (int) (yRender / Tile.SIZE) - offset;
		int xEnd = (Window.WIDTH / Tile.SIZE) + xStart + (offset * 2);
		int yEnd = (Window.HEIGHT / Tile.SIZE) + yStart + (offset * 2);
		
		for (int x = xStart; x < xEnd; x++) {
			for (int y = yStart; y < yEnd; y++) {
				if (solidTile(x, y)) {
					environment[x][y].draw(x * Tile.SIZE, y * Tile.SIZE, Tile.SIZE, Tile.SIZE); 
				}
			}
		}
	}
	
	public static void load(String path) throws Exception {
		
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(new InputStreamReader(World.class.getResourceAsStream(path)));
		JSONObject jObj = (JSONObject) obj;
		
		JSONArray layers = (JSONArray) jObj.get("layers");
		int amount = layers.size();
		
		for (int i = 0; i < amount; i++) {
            
			JSONObject layer = (JSONObject) layers.get(i);
			String type = (String) layer.get("name");
			
			if (type.equals("environment")) {
				WIDTH = ((Long) layer.get("width")).intValue(); //Weird solution to the orignal problem, but it works
				HEIGHT = ((Long) layer.get("height")).intValue();
				environment = parse((JSONArray) layer.get("data"));
			} else if (type.equals("enemy")) {
				//TODO Make enemies appear here ;)
			}
		}
	}
	
	private static Image[][] parse(JSONArray arr) {
		
		Image[][] layer = new Image[WIDTH][HEIGHT];
		int index;
		
		for (int x = 0; x < WIDTH; x++) {
			for (int y = 0; y < HEIGHT; y++) {
				index = ((Long) arr.get((y * WIDTH) + x)).intValue();
				layer[x][y] = getSpriteImage(index);
			}
		}
		
		return layer;
		
	}
	
	private static Image getSpriteImage(int index) {
		
		if (index == 0) return null;
		index -= 1;
		
		SpriteSheet sheet = Resources.getSprite("tileset");
		//int vertical = sheet.getVerticalCount(); //Unused for now, see below
		int horizontal = sheet.getHorizontalCount();
		
		int y = (index / horizontal); //The horizontal on this line was orignaly vertical. In order for the calculations to be correct using vertical, the tileset has to be a square. Using horizontal permits any rectangle displacement to also be used. Merci -Moustache
		int x = (index % horizontal);
		
		return sheet.getSubImage(x, y);
	}
	
	public static boolean inBounds(int x, int y) {
		return (x >= 0 && y >= 0 && x < WIDTH && y < HEIGHT);
	}
	
	public static boolean solidTile(int x, int y) {
		return (inBounds(x, y) && environment[x][y] != null);
	}
	
	public static boolean hitTest(float x, float y) {
		
		int xPoint = (int) ((x / Tile.SCALE) % Tile.SMALL_SIZE);
		int yPoint = (int) ((y / Tile.SCALE) % Tile.SMALL_SIZE);
		int xTile = (int) (x / Tile.SIZE);
		int yTile = (int) (y / Tile.SIZE);
		
		if (solidTile(xTile, yTile)) {
			return (environment[xTile][yTile].getColor(xPoint, yPoint).a > 0);
		}
		
		return false;
	}
}

