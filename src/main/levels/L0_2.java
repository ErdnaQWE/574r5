//Utilise pour tester les systeme
//Exemple d'un niveau typique

package main.levels;

import main.instances.enemies.Boss_Brains;
import main.states.GameState;

public class L0_2 extends Level {
	
	public String name = "L0_2"; //Nom du niveau (Si il faut afficher au dessus des points ou au debut du niveau) TODO
	
	@Override
	public void update(int delta) { //Cette fonction sera appele par l'Update loop de GameState
		
		time += delta; //Temps depuis le dernier update
		
		while (time >= next) { //Detecte si c'est le temps d'un evenement. Utilise une boucle while pour spawner tout les evenements si le jeu lag hard!!!
			
			System.out.println("Time is " + time);
			time -= next; //Si oui, enleve le temps attendu 
		
			switch (next_num) {
			
			case 0:
				GameState.instances.add(new Boss_Brains(0,0));
				next = 5000;
				break;
			}
			
			next_num+=1;
		
		}
		
	}

}
