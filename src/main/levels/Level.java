//Format d'un niveau

package main.levels;

import main.instances.enemies.Brains_mini;
import main.states.GameState;

public class Level {
	
	protected int time = 0; //Temps depuis le dernier evenement du niveau, en millisecondes
	protected int next = 0; //Temps auquel le prochain enemie/evenement apparait, en millisecondes
	protected int next_num = 0; //Numero de l'evenement (Pour eviter d'avoir a faire un million de ifs)
	//protected int next_id = 0; //Id du monstre a invoquer (Pour eviter d'avoir besoin de faire du copier coller comme un defonce mental)
	
	
	public void update (int delta) {
		System.out.println("No update method was specified!");
		System.out.println("Nothing will happen...");
	}
	
	public void summon (int id, int x, int y) {
		//Monstres de base (Valeurs par defaut)
		//Pour invoquer des monstres differents (specials), il faut override cette fonction dans la fonction de ton niveau
		//Utilise super.summon pour quand meme etre capable de summon des monstres de base.
		
		switch (id) {
		
		case 1: //Brains_mini
			GameState.instances.add(new Brains_mini(x, y, 32, 32));
			break;
		default: //Error
			System.out.println ("Monster with id " + id + " does not exist and could not be summoned");
			break;
		}
		
		//Lag compensation (Pour que les monstres ne s'empilent pas l'un sur l'autre apres un freeze)
		GameState.instances.get(GameState.instances.size() - 1).update(time);		
	}
	
}
