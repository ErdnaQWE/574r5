/* Mode Arcade Original
C'est le mode arcade anciennement utilise pour la presentation du jeux (lors de sa creation)
C'etait le "niveau" compris dans la version donne a l'enseignant

Les monstres apparaiteront en "vagues"
A chaque nouvelle vague, le montant de monstres par interval augmente de 1
A chaque interval de temps, le montant specifie de monstres apparait pour l'interval diminue
A chaque fois que l'interval atteint un certain montant, c'est la fin d'une vague. L'interval retourne a sa valeur initiale
 
*/

package main.levels;

import main.Window;
import main.instances.enemies.Brains_mini;
import main.instances.enemies.Invader;
import main.instances.enemies.Shooter;
import main.instances.enemies.Speedster;
import main.states.GameState;

public class LArcade extends Level {
	
	private int montant = 1; //Nombres de monstres par interval
	
	public LArcade () {
		time = 2500; //Temps en millisecondes depuis le dernier changement d'interval (pour l'apparition des monstres). La valeur par defaut de 2500 reduit le temps d'attent entre le premier monstre et le debut de la partie.
		next = 5000; //Interval entre les apparitions des monstres
	}

	public void update (int delta) {
		
		time += delta;
		
		if (time >= next) {
			time -= next;
			next -= next * 0.05; //Reduit le temps avant l'apparition du prochain monstre
			
			if (next < 500) { //Quand le delai d'apparition est rendu trop petit, on passe a la prochaine vague
				montant++;
				next = 5000;
			}
			
			for (int i = 0; i < montant; i++) {
				switch ((int) Math.floor(Math.random() * 4) + 1) {
				
				case 1:
					GameState.instances.add(new Brains_mini((int) Math.round(Math.random()*(Window.WIDTH - 200) + 100), (int) Math.round(-(Math.random() * 200) - 32), 32, 32));
					break;
				
				case 2:
					GameState.instances.add(new Invader((int) Math.round(Math.random()* 18 * 32 + 12), 0, 64, 64));
					break;

				case 3:
					GameState.instances.add(new Shooter((int) Math.round(Math.random()*Window.WIDTH), (int) Math.round(-(Math.random() * 200) - 64), 64, 64));
					break;
					
				case 4:
					GameState.instances.add(new Speedster((int) Math.round(Math.random()*Window.WIDTH), (int) Math.round(-(Math.random() * 200) - 64), 64, 64));
					break;
				
				}
			}
		}
		
	}

}
