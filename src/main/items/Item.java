package main.items;

import main.Info;
import main.utilities.Box;

public abstract class Item extends Box {
	
	public String name = "Null";
	public String description = "This item has no description.";
	
	
	public abstract void collect(Info info);//Immediate effect for when the object is collected.

	public void update (int delta) {
		
		//Basic object movement (If I decide to do some dpeending on how items are handed out)
		
	}
	

}
