package main.items;

import org.newdawn.slick.Image;

import main.Info;
import main.Resources;

public class Spoon_001 extends Item {

	public String name = "Spoon";
	public String description = "The soup's best friend";
	public Image Sprite = Resources.getImage("spoon");
	
	@Override
	public void collect(Info info) {
		info.min_damage += 1;
		info.max_damage += 1;
	}

}
